from models import Statement, Article, BaseStatement, Source


def print_fn(level, statement: BaseStatement, tag, source):
    print(f'{" " * level * 2 + "┗━" if level else ""}{tag}', f'достоверность: {statement.cost}', f'источник: {source}')


def print_article(article: Article):
    print_fn(0, article, '[Публикация]', article.source.cost)
    for statement in article.statements:
        print_statement(1, statement, '[Утверждение]')


def print_statement(level, statement: Statement, tag):
    print_fn(level, statement, tag, statement.source.cost)
    if statement.has_statements:
        print_children(statement, level + 1)


def print_children(statement: Statement, level: int):
    for proof in statement.proofs:
        print_statement(level, proof, '[За]')
    for disproof in statement.disproofs:
        print_statement(level, disproof, '[Против]')


bob = Source('0.9')
doc = Source('0.8')
speak = Source('0.2')

speaker = Source('1')

root = Article(speaker)

stA = Statement(speaker)
stB = Statement(speaker)
root.add_statement(stA)
root.add_statement(stB)

# sta11 = Statement(bob)
sta12 = Statement(speak)
# stA.add_disproof(sta11)
stA.add_disproof(sta12)

sta21 = Statement(doc)
sta22 = Statement(speak)
sta12.add_disproof(sta21)
sta12.add_proof(sta22)

print_article(root)
