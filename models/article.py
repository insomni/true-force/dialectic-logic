from decimal import Decimal
from functools import reduce
from typing import List

from .statement import Statement
from .base_statement import BaseStatement
from .source import Source


class Article(BaseStatement):

    def __init__(self, source: Source):
        super().__init__(source)
        self._statements: List[Statement] = []

    def add_statement(self, statement: Statement):
        self._statements.append(statement)
        statement.add_target(self)
        self.set_relevant(False)

    def set_relevant(self, relevant: bool = True):
        self._relevant = relevant

    @property
    def source(self):
        return self._source

    @property
    def statements(self):
        return self._statements

    @property
    def cost(self):
        if not self._relevant:
            self.calculate()
        return self._cost

    def calculate(self):
        if self._statements:
            cost = reduce(lambda prev, cur: prev + cur.cost, self._statements, 0) / len(self._statements)
        else:
            cost = 1
        self._cost = Decimal(str(cost)) * Decimal(str(self._source.cost))
        self.set_relevant()
