from decimal import Decimal

from .source import Source


class BaseStatement(object):

    def __init__(self, source: Source):
        self._cost: Decimal = None
        self._relevant: bool = False
        self._source: Source = source

    def set_relevant(self, relevant: bool):
        raise NotImplementedError

    def calculate(self):
        raise NotImplementedError

    @property
    def source(self):
        raise NotImplementedError

    @property
    def cost(self):
        raise NotImplementedError
