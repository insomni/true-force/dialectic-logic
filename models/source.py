from decimal import Decimal
from typing import List


class Source(object):

    def __init__(self, base: str):
        self._cost: Decimal = None
        self._relevant: bool = False
        self._articles: List = []
        self._base = Decimal(base)

    @property
    def cost(self):
        if not self._relevant:
            self.calculate()
        return self._cost

    @property
    def articles(self):
        return self._articles

    def set_relevant(self, relevant: bool = True):
        self._relevant = relevant

    def calculate(self):
        self._cost = self._base
        self.set_relevant()


