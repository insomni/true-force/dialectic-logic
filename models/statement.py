from decimal import Decimal
from typing import List

from .base_statement import BaseStatement
from .source import Source


class Statement(BaseStatement):
    """суждение"""

    def __init__(self, source: Source):
        super().__init__(source)
        self._proofs: List[Statement] = []  # массив доказательств
        self._disproofs: List[Statement] = []  # массив опровержений
        self._targets: List[BaseStatement] = []  # массив родителей

    def add_target(self, target: BaseStatement):
        self._targets.append(target)

    def add_proof(self, proof: 'Statement'):
        """добавить доказательство"""
        self._proofs.append(proof)
        proof.add_target(self)
        self.set_relevant(False)

    def add_disproof(self, disproof: 'Statement'):
        """добавить опровержение"""
        self._disproofs.append(disproof)
        disproof.add_target(self)
        self.set_relevant(False)

    def set_relevant(self, relevant: bool = True):
        """установить актуальность"""
        self._relevant = relevant
        if not relevant:
            for target in self._targets:
                target.set_relevant(False)

    @property
    def source(self):
        return self._source

    @property
    def cost(self) -> float:
        """стоимость утверждения"""
        if not self._relevant:
            self.calculate()
        return self._cost

    @property
    def has_statements(self):
        return self.count_statements > 0

    @property
    def count_statements(self):
        """общее количество утверждений"""
        return len(self._proofs) + len(self._disproofs)

    @property
    def proofs(self):
        return self._proofs

    @property
    def disproofs(self):
        return self._disproofs

    def calculate(self):
        """расчет стоимости"""
        if self.count_statements == 0:
            cost = Decimal('1')
        elif not self._proofs:
            cost = Decimal('1') - max(map(lambda a: a.cost * a.source.cost, self._disproofs))
        elif not self._disproofs:
            cost = max(map(lambda a: a.cost * a.source.cost, self._proofs))
        else:
            true = max(map(lambda a: a.cost * a.source.cost, self._proofs))
            false = Decimal('1') - max(map(lambda a: a.cost * a.source.cost, self._disproofs))
            cost = (true + false) / 2
        self._cost = Decimal(cost)
        self.set_relevant()
