import unittest
from decimal import Decimal

from models import Statement, Article, Source


class CaseExample(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.original = Source('1')
        cls.bob = Source('0.9')
        cls.doc = Source('0.8')
        cls.speak = Source('0.2')
        cls.statements = {}
        cls.article = Article(cls.original)

    def test_01(self):
        # Инициализация утверждений
        self.article.add_statement(Statement(self.original))
        self.article.add_statement(Statement(self.original))
        print('# Инициализация утверждений', self.article.cost)
        self.assertEqual(self.article.cost, Decimal('1'))

    def test_02(self):
        # Боб купил кефир
        self.article.statements[1].add_disproof(Statement(self.speak))
        print('# Боб купил кефир', self.article.cost)
        self.assertEqual(self.article.cost, Decimal('0.9'))

    def test_03(self):
        # В магазине не было кефира
        self.article.statements[1].disproofs[0].add_disproof(Statement(self.doc))
        print('# В магазине не было кефира', self.article.cost)
        self.assertEqual(self.article.cost, Decimal('0.98'))

    def test_04(self):
        # Дома у Боба стоит кефир
        self.article.statements[1].disproofs[0].add_proof(Statement(self.speak))
        print('# Дома у Боба стоит кефир', self.article.cost)
        self.assertEqual(self.article.cost, Decimal('0.98'))

    def test_05(self):
        # Боб ничего не купил
        self.article.statements[1].add_disproof(Statement(self.bob))
        print('# Боб ничего не купил', self.article.cost)
        self.assertEqual(self.article.cost, Decimal('0.55'))


if __name__ == '__main__':
    unittest.main()
